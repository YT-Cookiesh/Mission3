<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StageRepository")
 */
class Stage
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(name="idStage", type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="dateStage", type="date", nullable=true)
     */
    private $dateStage;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Stage", inversedBy="usereleve")
     * @ORM\JoinColumn(nullable=true)
     * @ORM\Column(name="idUserEleve", type="integer")
     */
    private $idUserEleve;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Stage", inversedBy="userprof")
     * @ORM\JoinColumn(nullable=true)
     * @ORM\Column(name="idUserProf", type="integer")
     */
    private $idUserProf;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Stage", inversedBy="tuteur")
     * @ORM\JoinColumn(nullable=true)
     * @ORM\Column(name="idTuteur", type="integer")
     */
    private $idTuteur;

    public function getId()
    {
        return $this->id;
    }

    public function getDateStage(): ?\DateTimeInterface
    {
        return $this->dateStage;
    }

    public function setDateStage(?\DateTimeInterface $dateStage): self
    {
        $this->dateStage = $dateStage;

        return $this;
    }

    public function getIdUserEleve(): ?int
    {
        return $this->idUserEleve;
    }

    public function setIdUserEleve(int $idUserEleve): self
    {
        $this->idUserEleve = $idUserEleve;

        return $this;
    }

    public function getIdUserProf(): ?int
    {
        return $this->idUserProf;
    }

    public function setIdUserProf(int $idUserProf): self
    {
        $this->idUserProf = $idUserProf;

        return $this;
    }

    public function getIdTuteur(): ?int
    {
        return $this->idTuteur;
    }

    public function setIdTuteur(int $idTuteur): self
    {
        $this->idTuteur = $idTuteur;

        return $this;
    }
}
