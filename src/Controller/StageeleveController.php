<?php

namespace App\Controller;

use App\Entity\Entreprise;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class StageeleveController extends Controller
{
    /**
     * @Route("/stageeleve", name="stageeleve")
     */
    public function index()
    {
        $stagee = $this->getDoctrine()
            ->getRepository(Entreprise::class)
            ->findAll();
        return $this->render('stageeleve/stageeleve.html.twig', compact('stagee'));
    }

}
