<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AideeleveController extends Controller
{
    /**
     * @Route("/aideeleve", name="aideeleve")
     */
    public function index()
    {
        return $this->render('aideeleve/aideeleve.html.twig', [
            'controller_name' => 'AideeleveController',
        ]);
    }
}
