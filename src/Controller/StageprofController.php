<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class StageprofController extends Controller
{
    /**
     * @Route("/stageprof", name="stageprof")
     */
    public function index()
    {
        return $this->render('stageprof/stageprof.html.twig', [
            'controller_name' => 'StageprofController',
        ]);
    }
}
