<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ProfesseurController extends Controller
{
    /**
     * @Route("/professeur", name="professeur")
     */
    public function index()
    {
        return $this->render('professeur/profaccueil.html.twig', [
            'controller_name' => 'ProfesseurController',
        ]);
    }
}
